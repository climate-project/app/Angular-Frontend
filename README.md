# Angular with PrimeNG
[Prime NG](https://www.primefaces.org/primeng/#/)

# Version
```
➜ ng --version
Angular CLI: 8.0.1
Node: 10.16.0
OS: linux x64
Angular: 8.0.0

➜ npm -v
6.9.0

➜ node -v
v10.16.0
```

## Install OpenLayers
[OpenLayers Doc](https://openlayers.org/en/latest/doc/tutorials/bundle.html)
```
npm install ol
npm install --save-dev parcel-bundler
npm install @types/ol
```

## Install D3
```
npm install d3
npm install @types/d3
```

## Install Highchart
[wtf document](https://api.highcharts.com/highcharts/chart.type)
```
npm install highcharts
```

### Shortcut npm install
```
npm install ol @types/ol d3 @types/d3 highcharts @angular/material @angular/cdk @angular/animations primeng primeicons
```

### OpenLayers Map Graticule
[graticule](https://openlayers.org/en/latest/examples/graticule.html)

## Country GeoJson
[OpenLayers Select Feature](https://openlayers.org/en/latest/examples/select-features.html)

[world geo.json file](https://github.com/johan/world.geo.json)

[countries geojson format](https://datahub.io/core/geo-countries)

## Datamap
[Datamap](https://datamaps.co/)
