import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private messageSource = new BehaviorSubject<any>(undefined);
  currentDetails = this.messageSource.asObservable();
  changeDetails(message: any) {
    this.messageSource.next(message)
  }

  private messageSource2 = new BehaviorSubject<any>(undefined);
  currentLowRes = this.messageSource2.asObservable();
  changeLowRes(message: any) {
    this.messageSource2.next(message)
  }

  private messageSource3 = new BehaviorSubject<any>(undefined);
  currentHiRes = this.messageSource3.asObservable();
  changeHiRes(message: any) {
    this.messageSource3.next(message)
  }

  private messageSource4 = new BehaviorSubject<any>(undefined);
  currentChartData = this.messageSource4.asObservable();
  changeChartData(message: any) {
    this.messageSource4.next(message)
  }

  private messageSource5 = new BehaviorSubject<any>(undefined);
  currentCountryChartData = this.messageSource5.asObservable();
  changeCountryChartData(message: any) {
    this.messageSource5.next(message)
  }

  private messageSource6 = new BehaviorSubject<any>(undefined);
  currentTrendAnalysis = this.messageSource6.asObservable();
  changeTrendAnalysis(message: any) {
    this.messageSource6.next(message)
  }

  private messageSource7 = new BehaviorSubject<any>(undefined);
  currentMannkendall = this.messageSource7.asObservable();
  changeMannkendall(message: any) {
    this.messageSource7.next(message)
  }

  private messageSource8 = new BehaviorSubject<any>(undefined);
  currentGrid = this.messageSource8.asObservable();
  changeGrid(message: any) {
    this.messageSource8.next(message)
  }
  constructor() {}
}
