import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ExportService {
  private apiURL = 'http://localhost:3000/api'

  exportNCData(dataset, dataset_type, index, start_date, end_date): Observable<any>{
    return this.http.get(this.apiURL+'/nc/datasets/'+dataset+'/types/'+dataset_type+'/indices/'+index+'/'+start_date+'/'+end_date+'/exportcsv', { responseType: 'text' })
  }

  exportNCCountryData(dataset, dataset_type, index, start_date, end_date, country): Observable<any>{
    const httpOptions = {
      headers: new HttpHeaders({
        'country':  country,
      })
    };
    return this.http.get(this.apiURL+'/nc/datasets/'+dataset+'/types/'+dataset_type+'/indices/'+index+'/'+start_date+'/'+end_date+'/selectcountry/exportcsv"', httpOptions)
    }

  exportNCIndicesData(dataset, index, start_year, end_year): Observable<any>{
    return this.http.get(this.apiURL+'/ncindices/datasets/'+dataset+'/indices/'+index+'/'+start_year+'/'+end_year+'/exportcsv', { responseType: 'text' })
  }

  exportNCIndicesCountryData(dataset, index, start_year, end_year, country): Observable<any>{
    const httpOptions = {
      headers: new HttpHeaders({
        'country':  country,
      })
    };
    return this.http.get(this.apiURL+'/ncindices/datasets/'+dataset+'/indices/'+index+'/'+start_year+'/'+end_year+'/exportcsv', httpOptions)
  }

  constructor(private http: HttpClient) { }
}
