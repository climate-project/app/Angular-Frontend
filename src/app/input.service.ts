import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class InputService {
  private apiURL = 'http://localhost:3000/api'
  
  getDatasets(): Observable<any>{
    return this.http.get(this.apiURL + '/datasetsDetail/')
  }

  getDatasetsDetail(dataset): Observable<any>{
    return this.http.get(this.apiURL + '/datasetsDetail/'+dataset)
  }

  getIndexMeaning(index): Observable<any>{
    return this.http.get(this.apiURL + '/indicesDetail/'+index)
  }

  getData(dataset, index, startmonth, startyear, stopmonth, stopyear): Observable<any>{
    startyear = parseInt(startyear)
    stopyear = parseInt(stopyear)
    return this.http.get(this.apiURL+'/datasets/'+dataset+'/indices/'+index+'/'+startyear+'-'+startmonth+'/'+stopyear+'-'+stopmonth)
  }

  getMannKendall(dataset, index, startmonth, startyear, stopmonth, stopyear): Observable<any>{
    startyear = parseInt(startyear)
    stopyear = parseInt(stopyear)
    return this.http.get(this.apiURL+'/datasets/'+dataset+'/indices/'+index+'/'+startyear+'-'+startmonth+'/'+stopyear+'-'+stopmonth+'/mannkendall')
  }

  getTrendAnalysis(dataset, index, startmonth, startyear, stopmonth, stopyear, method): Observable<any>{
    startyear = parseInt(startyear)
    stopyear = parseInt(stopyear)
    return this.http.get(this.apiURL+'/datasets/'+dataset+'/indices/'+index+'/'+startyear+'-'+startmonth+'/'+stopyear+'-'+stopmonth+'/slope/'+method)
  }

  getNCData(dataset, dataset_type, index, start_date, end_date): Observable<any>{
    return this.http.get(this.apiURL+'/nc/datasets/'+dataset+'/types/'+dataset_type+'/indices/'+index+'/'+start_date+'/'+end_date)
  }

  getNCMannKendall(dataset, dataset_type, index, start_date, end_date): Observable<any>{
    return this.http.get(this.apiURL+'/nc/datasets/'+dataset+'/types/'+dataset_type+'/indices/'+index+'/'+start_date+'/'+end_date+'/mannkendall')
  }

  getNCTrendAnalysis(dataset, dataset_type, index, start_date, end_date, method): Observable<any>{
    return this.http.get(this.apiURL+'/nc/datasets/'+dataset+'/types/'+dataset_type+'/indices/'+index+'/'+start_date+'/'+end_date+'/slope/'+method)
  }

  getNCDataLowRes(dataset, dataset_type, index, start_date, end_date): Observable<any>{
    return this.http.get(this.apiURL+'/nc/datasets/'+dataset+'/types/'+dataset_type+'/indices/'+index+'/'+start_date+'/'+end_date+'/lowres')
  }

  getNCMannKendallLowRes(dataset, dataset_type, index, start_date, end_date): Observable<any>{
    return this.http.get(this.apiURL+'/nc/datasets/'+dataset+'/types/'+dataset_type+'/indices/'+index+'/'+start_date+'/'+end_date+'/mannkendall/lowres')
  }

  getNCTrendAnalysisLowRes(dataset, dataset_type, index, start_date, end_date, method): Observable<any>{
    return this.http.get(this.apiURL+'/nc/datasets/'+dataset+'/types/'+dataset_type+'/indices/'+index+'/'+start_date+'/'+end_date+'/slope/'+method+'/lowres')
  }

  getNCIndicesData(dataset, index, startyear, stopyear): Observable<any>{
    startyear = parseInt(startyear)
    stopyear = parseInt(stopyear)
    return this.http.get(this.apiURL+'/ncindices/datasets/'+dataset+'/indices/'+index+'/'+startyear+'/'+stopyear)
  }
  getNCIndicesMannKendall(dataset, index, startyear, stopyear): Observable<any>{
    startyear = parseInt(startyear)
    stopyear = parseInt(stopyear)
    return this.http.get(this.apiURL+'/ncindices/datasets/'+dataset+'/indices/'+index+'/'+startyear+'/'+stopyear+'/mannkendall')
  }

  getNCIndicesTrendAnalysis(dataset, index, startyear, stopyear, method): Observable<any>{
    startyear = parseInt(startyear)
    stopyear = parseInt(stopyear)
    return this.http.get(this.apiURL+'/ncindices/datasets/'+dataset+'/indices/'+index+'/'+startyear+'/'+stopyear+'/slope/'+method)
  }

  getNCIndicesDataLowRes(dataset, index, startyear, stopyear): Observable<any>{
    startyear = parseInt(startyear)
    stopyear = parseInt(stopyear)
    return this.http.get(this.apiURL+'/ncindices/datasets/'+dataset+'/indices/'+index+'/'+startyear+'/'+stopyear+'/lowres')
  }
  getNCIndicesMannKendallLowRes(dataset, index, startyear, stopyear): Observable<any>{
    startyear = parseInt(startyear)
    stopyear = parseInt(stopyear)
    return this.http.get(this.apiURL+'/ncindices/datasets/'+dataset+'/indices/'+index+'/'+startyear+'/'+stopyear+'/mannkendall/lowres')
  }

  getNCIndicesTrendAnalysisLowRes(dataset, index, startyear, stopyear, method): Observable<any>{
    startyear = parseInt(startyear)
    stopyear = parseInt(stopyear)
    return this.http.get(this.apiURL+'/ncindices/datasets/'+dataset+'/indices/'+index+'/'+startyear+'/'+stopyear+'/slope/'+method+'/lowres')
  }
  
  getGrid(dataset): Observable<any>{
    return this.http.get(this.apiURL + '/datasetsDetail/'+dataset+'/grid')
  }
  constructor(private http: HttpClient) { }
}
