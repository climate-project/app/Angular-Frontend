import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { InputService } from '../input.service';
import { ExportService } from '../export.service';
import { DatePipe } from '@angular/common';
import {SelectItem} from 'primeng/api';
import { saveAs } from 'file-saver';

@Component({
  selector: 'app-export',
  templateUrl: './export.component.html',
  styleUrls: ['./export.component.css']
})
export class ExportComponent implements OnInit {
  indices_datasets: any = [];
  selectedDataset:any = "None";
  selectedDatasetDetail:any = null;
  selectedIndex:any = "None";
  selectedStartMonth: any = "None";
  selectedStartYear: any = "None";
  selectedStopMonth: any = "None";
  selectedStopYear: any = "None";

  raw_datasets: any = [];
  selectedRawDataset:any;
  Types: SelectItem[];
  selectedType:any = {label: 'RCP 4.5', value: 'rcp45'};
  Indices: SelectItem[];
  selectedIndices:any;
  rangeYear: string;
  startdate: Date;
  stopdate: Date;

  dialog: boolean = false;
  showDialog() {
    this.dialog = true;
  }

  getDatasets(): void{
    this.inputService.getDatasets()
      .subscribe(datasets => {
        let that = this
        datasets.forEach(function(e) {
          if(e.dataset_type == "index"){that.indices_datasets = [...that.indices_datasets, e]}
          else if(e.dataset_type == "raw"){that.raw_datasets = [...that.raw_datasets, e]}
          else {that.indices_datasets = [...that.indices_datasets, e]}
        })
      });
  }

  getDatasetsDetail(dataset): void{
    this.inputService.getDatasetsDetail(dataset)
      .subscribe(data => this.selectedDatasetDetail = data);
  }


  Download_rcm() {
    var start_date = this.datePipe.transform(this.startdate,"yyyy-MM-dd")
    var end_date = this.datePipe.transform(this.stopdate,"yyyy-MM-dd")
    var Difference_In_Time = this.stopdate.getTime() - this.startdate.getTime(); 
    var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24); 
    if(this.selectedRawDataset.dataset == undefined || this.selectedIndices.value == undefined || this.startdate == undefined || this.stopdate == undefined || start_date > end_date) {
      console.log(this.selectedRawDataset.dataset ,this.selectedIndices.value, start_date,end_date )
      this.showDialog()
    } else {
      if(Difference_In_Days <= 366) {
        this.exportService.exportNCData(this.selectedRawDataset.dataset, this.selectedType.value, this.selectedIndices.value, start_date, end_date)
          .subscribe(data => this.saveToFileSystem(data))
      } else { this.showDialog() }
    }
  }

  Download_indices() {
    if(this.selectedDataset.dataset == undefined || this.selectedIndex.index == undefined || this.selectedStartYear == undefined || 
        this.selectedStopYear == undefined || this.selectedStartYear > this.selectedStopYear) {
      this.showDialog()
    } else {
      this.exportService.exportNCIndicesData(this.selectedDataset.dataset, this.selectedIndex.index, this.selectedStartYear, this.selectedStopYear)
        .subscribe(data => this.saveToFileSystem(data))
    }
    
  }

  private saveToFileSystem(data: any) {

    var blob = new Blob([data], {type: 'text/csv' })
    saveAs(blob, "myFile.csv");
  }

  constructor(private datePipe: DatePipe, private inputService: InputService, private exportService: ExportService, private http: HttpClient) {
    this.Indices = [
      {label: 'Precip', value: 'pr'},
      {label: 'Temp Average', value: 'tas'},
      {label: 'Temp Min', value: 'tasmin'},
      {label: 'Temp Max', value: 'tasmax'},
    ];
    this.Types = [
      {label: 'RCP 4.5', value: 'rcp45'},
      {label: 'RCP 8.5', value: 'rcp85'},
    ]
  }

  ngOnInit() {
    this.getDatasets()
  }

}
