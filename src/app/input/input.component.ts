import { Component, OnInit } from '@angular/core';
import { InputService } from '../input.service';
import { DataService } from '../data.service';
import { DatePipe } from '@angular/common';
import * as d3 from 'd3';
import {SelectItem} from 'primeng/api';
import { delay } from 'rxjs/operators';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.css']
})
export class InputComponent implements OnInit {
  indices_datasets: any = [];
  selectedDataset:any = "None";
  selectedDatasetDetail:any = null;
  selectedIndex:any = "None";
  selectedIndexMeaning: any = "None";
  selectedStartMonth: any = "None";
  selectedStartYear: any = "None";
  selectedStopMonth: any = "None";
  selectedStopYear: any = "None";
  selectedTrendMethod:any = {label: 'Theilsen', value: 'theilsen'};

  raw_datasets: any = [];
  selectedRawDataset:any = "None";
  Types: SelectItem[];
  selectedType:any = {label: 'RCP 4.5', value: 'rcp45'};
  Indices: SelectItem[];
  selectedIndices:any = "None";
  rangeYear: string;
  startdate: Date;
  stopdate: Date;
  selectedRCMTrendMethod:any = {label: 'Theilsen', value: 'theilsen'};

  Method: SelectItem[];

  details:any = {};
  dataLowRes:any = {};
  dataHiRes:any = {};
  dataTrendAnalysis:any = {};
  dataMannkendall:any = {};

  dialog: boolean = false;
  fix_legend: boolean = false;
  min_legend: string;
  max_legend: string;
  disabled: boolean = true;

  toggleDisabled() {
      this.disabled = !this.disabled;
  }
  showDialog() {
    this.dialog = true;
  }

  getDatasets(): void{
    this.inputService.getDatasets()
      .subscribe(datasets => {
        let that = this
        datasets.forEach(function(e) {
          if(e.dataset_type == "index"){that.indices_datasets = [...that.indices_datasets, e]}
          else if(e.dataset_type == "raw"){that.raw_datasets = [...that.raw_datasets, e]}
          else {that.indices_datasets = [...that.indices_datasets, e]}
        })
      });
  }

  getDatasetsDetail(dataset): void{
    this.inputService.getDatasetsDetail(dataset)
      .subscribe(data => this.selectedDatasetDetail = data);
  }

  Visualize() {
    var detail = {'source': this.selectedDataset.source,
    'dataset_type': this.selectedDataset.dataset_type,
    'dataset': this.selectedDataset.dataset,
    'start': this.selectedStartYear,
    'stop': this.selectedStopYear,}
  
    this.inputService.getIndexMeaning(this.selectedIndex.index).pipe().subscribe(res => {
      this.details = {...this.details, ...res[0]}
      this.details = {...this.details, ...detail}
      this.dataService.changeDetails(this.details)
    });
    this.inputService.getGrid(this.selectedDataset.dataset).subscribe(res => this.dataService.changeGrid(res))

    var legend = {
      'fix': this.fix_legend,
      'min': this.min_legend,
      'max': this.max_legend,
    }
    this.dataLowRes.legend = legend
    this.dataHiRes.legend = legend
    if(this.selectedDataset.dataset == undefined || this.selectedIndex.index == undefined || this.selectedStartYear == undefined || 
        this.selectedStopYear == undefined || this.selectedStartYear > this.selectedStopYear) {
      this.showDialog()
    } else {
      document.getElementById("spinner-front").classList.add("show");
      document.getElementById("spinner-back").classList.add("show");

      // Handle error call service multiple add same time
      // Get Low-Res data
      this.inputService.getNCIndicesDataLowRes(this.selectedDataset.dataset, this.selectedIndex.index, this.selectedStartYear, this.selectedStopYear)
      .pipe(delay(500)) // wait for grid
      .subscribe(data => {
        this.dataLowRes = {...this.dataLowRes, ...data}
        this.dataService.changeLowRes(this.dataLowRes) // Sending low-res data to map
        this.inputService.getNCIndicesMannKendallLowRes(this.selectedDataset.dataset, this.selectedIndex.index, this.selectedStartYear, this.selectedStopYear)
          .subscribe(mk => {
            this.dataMannkendall.mktest = mk.mannkendall
            this.dataMannkendall.resolution = 'lowres_trend'
            this.dataService.changeMannkendall(this.dataMannkendall) // Sending low-res Mannkendall to map
            this.inputService.getNCIndicesTrendAnalysisLowRes(this.selectedDataset.dataset, this.selectedIndex.index, this.selectedStartYear, this.selectedStopYear, this.selectedTrendMethod.value)
              .subscribe(theilsen => {
                this.dataTrendAnalysis.resolution = 'lowres_data'
                this.dataTrendAnalysis.data = theilsen
                this.dataService.changeTrendAnalysis(this.dataTrendAnalysis) // Sending low-res  trend analysis data to map 

                // Get Hi-Res data
                this.inputService.getNCIndicesData(this.selectedDataset.dataset, this.selectedIndex.index, this.selectedStartYear, this.selectedStopYear)
                  .subscribe(data => {
                    this.dataHiRes = {...this.dataHiRes, ...data}
                    this.dataService.changeHiRes(this.dataHiRes)  // Sending hi-res data to map
                    this.inputService.getNCIndicesMannKendall(this.selectedDataset.dataset, this.selectedIndex.index, this.selectedStartYear, this.selectedStopYear)
                      .subscribe(mk => {
                        this.dataMannkendall.mktest = mk.mannkendall
                        this.dataMannkendall.resolution = 'hires_trend'
                        this.dataService.changeMannkendall(this.dataMannkendall)  // Sending hi-res Mannkendall to map
                        this.inputService.getNCIndicesTrendAnalysis(this.selectedDataset.dataset, this.selectedIndex.index, this.selectedStartYear, this.selectedStopYear, this.selectedTrendMethod.value)
                          .subscribe(theilsen => {
                            this.dataTrendAnalysis.resolution = 'hires_data'
                            this.dataTrendAnalysis.data = theilsen
                            this.dataService.changeTrendAnalysis(this.dataTrendAnalysis) // Sending hi-res trend analysis data to map
                          })
                      })
                  })
              })
          })
      })

      
    }   
  }

  Visualize2() {
    var start_date = this.datePipe.transform(this.startdate,"yyyy-MM-dd")
    var end_date = this.datePipe.transform(this.stopdate,"yyyy-MM-dd")

    var detail = {'source': this.selectedRawDataset.source,
    'dataset_type': this.selectedRawDataset.dataset_type,
    'dataset': this.selectedRawDataset.dataset,
    'start': start_date,
    'stop': end_date,
    'rawtype': this.selectedType.value
    }
    this.inputService.getIndexMeaning(this.selectedIndices.value).pipe().subscribe(res => {
      this.details = {...this.details, ...res[0]}
      this.details = {...this.details, ...detail}
      this.dataService.changeDetails(this.details)
    });
    this.inputService.getGrid(this.selectedRawDataset.dataset).subscribe(res => this.dataService.changeGrid(res))

    var legend = {
      'fix': this.fix_legend,
      'min': this.min_legend,
      'max': this.max_legend,
    }
    this.dataLowRes.legend = legend
    this.dataHiRes.legend = legend
    if(this.selectedRawDataset.dataset == undefined || this.selectedIndices.value == undefined || this.startdate == undefined || this.stopdate == undefined || start_date > end_date) {
      this.showDialog()
    } else {
      document.getElementById("spinner-front").classList.add("show");
      document.getElementById("spinner-back").classList.add("show");

      // Get Low-Res data
      this.inputService.getNCDataLowRes(this.selectedRawDataset.dataset, this.selectedType.value, this.selectedIndices.value, start_date, end_date)
      .subscribe(data => {
        this.dataLowRes = {...this.dataLowRes, ...data}
        this.dataService.changeLowRes(this.dataLowRes)  // Sending low-res data to map
        this.inputService.getNCMannKendallLowRes(this.selectedRawDataset.dataset, this.selectedType.value, this.selectedIndices.value, start_date, end_date)
          .subscribe(mk => {
            this.dataMannkendall.mktest = mk.mannkendall
            this.dataMannkendall.resolution = 'lowres_trend'
            this.dataService.changeMannkendall(this.dataMannkendall)  // Sending low-res Mannkendall to map
            this.inputService.getNCTrendAnalysisLowRes(this.selectedRawDataset.dataset, this.selectedType.value, this.selectedIndices.value, start_date, end_date, this.selectedRCMTrendMethod.value)
              .subscribe(theilsen => {
                this.dataTrendAnalysis.resolution = 'lowres_data'
                this.dataTrendAnalysis.data = theilsen
                this.dataService.changeTrendAnalysis(this.dataTrendAnalysis) // Sending low-res trend analysis data to map
              })           
          })
      })

      // Get Hi-Res data
      this.inputService.getNCData(this.selectedRawDataset.dataset, this.selectedType.value, this.selectedIndices.value, start_date, end_date)
        .subscribe(data => {
          this.dataHiRes = {...this.dataHiRes, ...data}
          this.dataService.changeHiRes(this.dataHiRes) // Sending hi-res data to map
          this.inputService.getNCMannKendall(this.selectedRawDataset.dataset, this.selectedType.value, this.selectedIndices.value, start_date, end_date)
            .subscribe(mk => {
              this.dataMannkendall.mktest = mk.mannkendall
              this.dataMannkendall.resolution = 'hires_trend'
              this.dataService.changeMannkendall(this.dataMannkendall) // Sending hi-res Mannkendall to map
              this.inputService.getNCTrendAnalysis(this.selectedRawDataset.dataset, this.selectedType.value, this.selectedIndices.value, start_date, end_date, this.selectedRCMTrendMethod.value)
                .subscribe(theilsen => {
                  this.dataTrendAnalysis.resolution = 'hires_data'
                  this.dataTrendAnalysis.data = theilsen
                  this.dataService.changeTrendAnalysis(this.dataTrendAnalysis) // Sending hi-res trend analysis data to map
                })
            })
        })
    }
  }

  constructor(private datePipe: DatePipe, private inputService: InputService, private dataService: DataService) { 
    this.Indices = [
      {label: 'Precip', value: 'pr'},
      {label: 'Temp Average', value: 'tas'},
      {label: 'Temp Min', value: 'tasmin'},
      {label: 'Temp Max', value: 'tasmax'},
    ];
    this.Types = [
      {label: 'RCP 4.5', value: 'rcp45'},
      {label: 'RCP 8.5', value: 'rcp85'},
    ];
    this.Method = [
      {label: 'Theilsen', value: 'theilsen'},
      {label: 'Linear Regression', value: 'linearreg'},
      {label: 'Least Square', value: 'leastsquare'}
    ]
  }

  ngOnInit() {
    this.getDatasets();
    // this.selectedType = {label: 'RCP 4.5', value: 'rcp45'}
  }

}
