import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MapService {
  private apiURL = 'http://localhost:3000/api'

  getGrid(dataset): Observable<any>{
    return this.http.get(this.apiURL + '/datasetsDetail/'+dataset+'/grid')
  }

  getIndexSelectCountry(dataset, index, start, stop, country): Observable<any>{
    const httpOptions = {
      headers: new HttpHeaders({
        'country':  country,
      })
    };
    return this.http.get(this.apiURL + '/datasets/'+dataset+'/indices/'+index+'/'+start+'/'+stop+'/selectcountry', httpOptions)
  }

  getNCSelectCountry(dataset, dataset_type, index, start_date, end_date, country): Observable<any>{
    const httpOptions = {
      headers: new HttpHeaders({
        'country':  country,
      })
    };
    return this.http.get(this.apiURL+'/nc/datasets/'+dataset+'/types/'+dataset_type+'/indices/'+index+'/'+start_date+'/'+end_date+'/'+'selectcountry', httpOptions)
  }

  getNCIndicesSelectCountry(dataset, index, start_year, end_year, country): Observable<any>{
    const httpOptions = {
      headers: new HttpHeaders({
        'country':  country,
      })
    };
    return this.http.get(this.apiURL+'/ncindices/datasets/'+dataset+'/indices/'+index+'/'+start_year+'/'+end_year+'/'+'selectcountry', httpOptions)
  }
  constructor(private http: HttpClient) { }
}