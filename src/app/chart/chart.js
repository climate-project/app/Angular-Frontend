import * as Highcharts from 'highcharts';
var highchartsRegression = require("highcharts-regression")
highchartsRegression(Highcharts)

export function draw_seasonal_chart(target, chartdata, y_title='Temperature (°C)') {
    var chart = new Highcharts.chart(target, {
        chart: {
            type: 'line',
        },
        title: {
            text: 'Seasonal Average'
        },
        xAxis: {
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        },
        yAxis: {
            title: {
                text: y_title
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: false
                },
                enableMouseTracking: true
            }
        },
        series: [{
            name: 'Global',
            data: chartdata
        }],
        credits: {
            enabled: false
        }
    })
    return chart
}

export function draw_global_chart(target, chartdata, yeararray, y_title='Temperature (°C)', series_name='Global') {
    var chart = new Highcharts.chart(target, {
        chart: {
            type: 'line',
            events: {
                load: function() {
                    this.series[1].update({
                        enableMouseTracking: false
                    })
                }
            }
        },
        title: {
            text: 'Annual Average'
        },
        xAxis: {
            categories: yeararray
        },
        yAxis: {
            title: {
                text: y_title
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: false
                },
                enableMouseTracking: true
            }
        },
        series: [{
            regression: true,
            regressionSettings: {
                type: 'linear',
                color: 'rgba(0,0,0,0.5)',
                name: 'Trend'
            },
            name: series_name,
            data: chartdata,
        }],
        credits: {
            enabled: false
        },
    })
  return chart
}

export function addSerie(target, name, data) {
    if(target.series.length < 5){
        target.addSeries({
            name: name,
            data: data
        });
    } else {
        target.series[2].remove()
        target.addSeries({
            name: name,
            data: data
        });
    }

}