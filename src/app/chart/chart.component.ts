import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import * as ChartLib from './chart.js';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})
export class ChartComponent implements OnInit {
  constructor(private dataService: DataService) { }
  annual_chart: any;
  seasonal_chart: any;
  country: any;
  Data(data){
    var new_data = [];
    for(var i=0; i < data.length;i++){
      new_data.push([i,data[i]])
    }
    return new_data
  }

  ngOnInit() {
    console.log('Chart init')
    this.dataService.currentChartData.subscribe(res => {
      if(res && res.global_avg.length > 0) {
        this.annual_chart = ChartLib.draw_global_chart('container1', this.Data(res.global_avg), res.year, res.yAxis)
      }
      if(res && res.seasonal_avg.length > 0) {
        this.seasonal_chart = ChartLib.draw_seasonal_chart('container2', this.Data(res.seasonal_avg), res.yAxis)
      }
    })

    this.dataService.currentCountryChartData.subscribe(res => {
      if(res && res.country != this.country) {
        this.country = res.country
        if(res && res.global_avg.length > 0) {
          ChartLib.addSerie(this.annual_chart, res.country, res.global_avg)
        }
        if(res && res.seasonal.length > 0) {
          ChartLib.addSerie(this.seasonal_chart, res.country, res.seasonal)
        }
      }
      
    })
  }
}
