import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';  // NgModel
import { HttpClientModule } from '@angular/common/http';
import {DatePipe} from '@angular/common';

// prime ng
import { ButtonModule } from 'primeng/button';
import { DropdownModule } from 'primeng/dropdown';
import { PanelModule } from 'primeng/panel';
import { RadioButtonModule } from 'primeng/radiobutton';
import {SidebarModule} from 'primeng/sidebar';
import {AccordionModule} from 'primeng/accordion';
import {CalendarModule} from 'primeng/calendar';
import {DialogModule} from 'primeng/dialog';
import {CheckboxModule} from 'primeng/checkbox';
import {KeyFilterModule} from 'primeng/keyfilter';
import {ProgressSpinnerModule} from 'primeng/progressspinner';
import {ToggleButtonModule} from 'primeng/togglebutton';
import {FieldsetModule} from 'primeng/fieldset';
import {ToastModule} from 'primeng/toast';

import { AppComponent } from './app.component';
import { InputComponent } from './input/input.component';
import { MapComponent } from './map/map.component';
import { ChartComponent } from './chart/chart.component';
import { ExportComponent } from './export/export.component';

@NgModule({
  declarations: [
    AppComponent,
    InputComponent,
    MapComponent,
    ChartComponent,
    ExportComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    ButtonModule,
    DropdownModule,
    PanelModule,
    RadioButtonModule,
    SidebarModule,
    AccordionModule,
    CalendarModule,
    DialogModule,
    CheckboxModule,
    KeyFilterModule,
    ProgressSpinnerModule,
    ToggleButtonModule,
    FieldsetModule,
    ToastModule
  ],
  providers: [DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
