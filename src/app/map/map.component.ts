import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { MapService } from '../map.service';
import {MessageService} from 'primeng/api';
import * as MapLib from './lib/map.js';
import * as MapInteractive from './lib/interactive.js';
import * as $ from "jquery";
import { forkJoin, Observable } from 'rxjs';
import { zip, map } from 'rxjs/operators';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css'],
  providers: [MessageService]
})
export class MapComponent implements OnInit {
  main_map: any;
  anomaly_map: any;
  trend_analysis_map: any;
  lowres_layer: any;
  hires_layer: any;
  lowres_mannkendall_layer: any;
  hires_mannkendall_layer: any;
  lowres_trend_analysis_layer: any;
  hires_trend_analysis_layer: any;

  details: any;
  data_lowres: any;
  data_hires: any;
  grid_lowres: any;
  grid_hires: any;

  select: any = null;

  chart: any;

  info: boolean = false;
  info_btn:boolean = false;
  selectedTrend:boolean = true;
  resolution:boolean = false;
  mode:boolean = false;
  disabled:boolean = true;
  
  constructor(private dataService: DataService, private mapService: MapService, private messageService: MessageService) { }

  showDialog() {
    this.info = true;
  }
  getDataLayer(details, data, grid, layername, map) {
    var layer, geojson
    if(map == "main") {
      geojson = MapLib.merge_data_to_geojson(grid.geojson_gridcenter, data.map.avg_map.data, 'value')
      if(data.legend.fix){
        layer = MapLib.draw_data({
            color_map: details.color_map, 
            geojson: geojson, 
            lat_step: grid.gridsize.lat_step, 
            lon_step: grid.gridsize.lon_step, 
            min: data.legend.min, 
            max: data.legend.max, 
            legendtarget: map, 
            layername: layername, 
            legend_title: this.details.index + ' (' +  this.details.unit + ')',
            legend_fix: data.legend.fix
          });
      } else {
        layer = MapLib.draw_data({
            color_map: details.color_map, 
            geojson: geojson, 
            lat_step: grid.gridsize.lat_step, 
            lon_step: grid.gridsize.lon_step, 
            min: data.map.avg_map.boxplot_range[0], 
            max: data.map.avg_map.boxplot_range[1], 
            legendtarget: map, 
            layername: layername, 
            legend_title: this.details.index + ' (' +  this.details.unit + ')'
          });
      }
    } else if(map == "trend") {
      if(data.resolution == "lowres_data"){
        geojson = MapLib.merge_data_to_geojson(grid.geojson_gridcenter, data.data.slope, 'value')
        layer = MapLib.draw_data({
            color_map: details.color_map, 
            geojson: geojson, 
            lat_step: grid.gridsize.lat_step, 
            lon_step: grid.gridsize.lon_step, 
            min: data.data.boxplot_range[0], 
            max: data.data.boxplot_range[1], 
            legendtarget: map, 
            layername: layername, 
            legend_title: this.details.index + ' (' +  this.details.unit + ')'
          });
      } else if(data.resolution == "hires_data"){
          geojson = MapLib.merge_data_to_geojson(grid.geojson_gridcenter, data.data.slope, 'value')
          layer = MapLib.draw_data({
            color_map: details.color_map, 
            geojson: geojson, 
            lat_step: grid.gridsize.lat_step, 
            lon_step: grid.gridsize.lon_step, 
            min: data.data.boxplot_range[0], 
            max: data.data.boxplot_range[1], 
            legendtarget: map, 
            layername: layername, 
            legend_title: this.details.index + ' (' +  this.details.unit + ')'
          });
      }
    }
    return layer
  }
  getTrendLayer(data, grid, layername) {
    var geojson = MapLib.merge_data_to_geojson(grid.geojson_gridcenter, data.mktest, 'trend')
    var layer = MapLib.draw_trend(geojson, layername);
    return layer
  }

  displayTrend(){
    if(this.selectedTrend) {
      this.main_map.getLayers().forEach(function (layer) {
        if (layer.get('name') == 'lowres_trend') {
          layer.setVisible(true)
        }
        else if (layer.get('name') == 'hires_trend') {
          layer.setVisible(true)
        }
      });
    } else {
      this.main_map.getLayers().forEach(function (layer) {
        if (layer.get('name') == 'lowres_trend') {
          layer.setVisible(false)
        }
        else if (layer.get('name') == 'hires_trend') {
          layer.setVisible(false)
        }
      });
    }
  }

  ChangeResolution(){
    if(this.resolution) {
      this.main_map.getLayers().forEach(function (layer) {
        if (layer.get('name') == 'lowres_data') {
          layer.setVisible(false)
        }
        else if (layer.get('name') == 'lowres_trend') {
          layer.set('maxZoom', 0)
        }
        else if (layer.get('name') == 'hires_data') {
          layer.set('minZoom', 0)
          layer.set('maxZoom', 12)
        }
        else if (layer.get('name') == 'hires_trend') {
          layer.set('minZoom', 0)
          layer.set('maxZoom', 12)
        }
      });
      this.trend_analysis_map.getLayers().forEach(function (layer) {
        if (layer.get('name') == 'lowres_data') {
          layer.setVisible(false)
        }
        else if (layer.get('name') == 'hires_data') {
          layer.set('minZoom', 0)
          layer.set('maxZoom', 12)
        }
      });
    } else {
      MapLib.setDataResolution(this.main_map)
      MapLib.setDataResolution(this.trend_analysis_map)
      MapLib.setTrendResolution(this.main_map)
      MapLib.setTrendResolution(this.trend_analysis_map)
    }
  }

  ChangeMode() {
    if(this.mode) {
      $("#chart").css("display", "none");
      $("#theilsen").css("display", "block");
    } else {
      $("#chart").css("display", "block");
      $("#theilsen").css("display", "none");
    }
  }

  ngOnInit() {
    console.log("Map calling Oninit")
    this.main_map = MapLib.draw_map('map');
    // this.anomaly_map = MapLib.draw_map('anomaly_map');
    this.trend_analysis_map = MapLib.draw_map('trend_map');
    $("#theilsen").css("display", "none");
    // MapLib.draw_graticule(this.main_map)

    this.dataService.currentDetails.subscribe(res => {
      this.details = res;
      this.disabled = true
      if(res){
        // Display description
        if(this.details.rawtype != undefined){document.getElementById('dataset').innerHTML = 'Dataset: ' + this.details.dataset + '&nbsp;' + this.details.rawtype}
        else{document.getElementById('dataset').innerHTML = 'Dataset: ' + this.details.dataset}
        document.getElementById('index').innerHTML = 'Index: ' + this.details.index
        if(this.details.short_definition != undefined){ document.getElementById('short_definition').innerHTML = 'Short_definition: ' + this.details.short_definition }
        this.info_btn = true
        document.getElementById('period').innerHTML = 'Period: ' + this.details.start + ' to ' + this.details.stop
      }
    }, err => {
      console.log(err);
    });

    // get grid
    this.dataService.currentGrid.subscribe(res => {
      if(res){
        this.grid_lowres = res[0]['lowres']
        this.grid_hires = res[0]
      }
    })

    // Low-Res data
    this.dataService.currentLowRes.subscribe(res => {
      if(res){
        console.log('2222222',res)
        this.data_lowres = res
        // get layer for add to map
        this.lowres_layer = this.getDataLayer(this.details, this.data_lowres, this.grid_lowres, 'lowres_data', 'main')
        
        MapLib.clearLayers(this.main_map);
        this.main_map.getLayers().insertAt(0,this.lowres_layer);
        this.main_map.getView().setZoom(4)
        this.main_map.getView().setCenter([115, 5])

        // Display stat
        document.getElementById('stat_avg').innerHTML = 'Avg: ' + this.data_lowres.map.avg_map.avg + '&nbsp;' +  this.details.unit
        document.getElementById('stat_min').innerHTML = 'Min: ' + this.data_lowres.map.avg_map.min + '&nbsp;' +  this.details.unit
        document.getElementById('stat_max').innerHTML = 'Max: ' + this.data_lowres.map.avg_map.max + '&nbsp;' +  this.details.unit
        document.getElementById('stat_variance').innerHTML = 'Variance: ' + this.data_lowres.map.avg_map.variance + '&nbsp;' +  this.details.unit

        // Chart data
        this.chart = {
          'global_avg': this.data_lowres.chart.global_avg,
          'seasonal_avg': this.data_lowres.chart.seasonal,
          'year': this.data_lowres.chart.year,
          'yAxis': this.details.index + ' (' + this.details.unit + ')',
        }
        this.dataService.changeChartData(this.chart)

        // Get selected country data chart
        if (this.select !== null) {
          this.main_map.removeInteraction(this.select);
        }
        let that = this
        this.select = MapInteractive.select_country(this.main_map, this.details.unit);
        this.select.on('select', function(e) {
          // Checking if not select country change to global stat
          if(e.selected.length == 0) {
            document.getElementById('country').innerHTML = '';
            document.getElementById('stat_avg').innerHTML = 'Avg: ' + that.data_hires.map.avg_map.avg + '&nbsp;' + that.details.unit
            document.getElementById('stat_min').innerHTML = 'Min: ' + that.data_hires.map.avg_map.min + '&nbsp;' + that.details.unit
            document.getElementById('stat_max').innerHTML = 'Max: ' + that.data_hires.map.avg_map.max + '&nbsp;' + that.details.unit
            document.getElementById('stat_variance').innerHTML = 'Variance: ' + that.data_hires.map.avg_map.variance + '&nbsp;' +  that.details.unit
          }
          else if(e.selected.length == 1) {
            var selectedCountry = e.selected[0].get('name');
            if(that.details.source == 'nc'){
              if(that.details.dataset_type == 'raw'){
                that.mapService.getNCSelectCountry(that.details.dataset, that.details.rawtype, that.details.index, that.details.start, that.details.stop, selectedCountry)
                  .subscribe(res => that.dataService.changeCountryChartData(res))
              } 
              else if(that.details.dataset_type == 'index'){
                that.mapService.getNCIndicesSelectCountry(that.details.dataset, that.details.index, that.details.start, that.details.stop, selectedCountry)
                  .subscribe(res => that.dataService.changeCountryChartData(res))
              }
            }
            else if(that.details.source == 'mongodb'){
              that.mapService.getIndexSelectCountry(that.details.dataset, that.details.index, that.details.start, that.details.stop, selectedCountry)
                .subscribe(res => that.dataService.changeCountryChartData(res))
            }
          }
        });

        // hide loading screen
        document.getElementById("spinner-front").classList.remove("show");
        document.getElementById("spinner-back").classList.remove("show");
        // notification
        this.messageService.add({severity:'success', summary:'Low-Res Data Available', detail:'Render Completed'});
        this.messageService.add({severity:'info', summary: 'Loading Hi-Res Data', detail:'Please wait', life:3000});
        this.messageService.add({severity:'info', summary: 'Loading Mannkendall(Trend) Data', detail:'Please wait', life:3000});
        this.messageService.add({severity:'info', summary: 'Loading Trend Analysis Data', detail:'Please wait', life:3000});
      }
    })

    // Hi-Res data
    this.dataService.currentHiRes.subscribe(res => {
      if(res){
        console.log('4444444',res)
        this.data_hires = res

        // Chart data
        this.chart = {
          'global_avg': this.data_hires.chart.global_avg,
          'seasonal_avg': this.data_hires.chart.seasonal,
          'year': this.data_hires.chart.year,
          'yAxis': this.details.index + ' (' + this.details.unit + ')',
        }
        this.dataService.changeChartData(this.chart)

        // Display stat
        document.getElementById('stat_avg').innerHTML = 'Avg: ' + this.data_hires.map.avg_map.avg + '&nbsp;' + this.details.unit
        document.getElementById('stat_min').innerHTML = 'Min: ' + this.data_hires.map.avg_map.min + '&nbsp;' + this.details.unit
        document.getElementById('stat_max').innerHTML = 'Max: ' + this.data_hires.map.avg_map.max + '&nbsp;' + this.details.unit
        document.getElementById('stat_variance').innerHTML = 'Variance: ' + this.data_hires.map.avg_map.variance + '&nbsp;' +  this.details.unit

        // Get data layer
        this.hires_layer = this.getDataLayer(this.details, this.data_hires, this.grid_hires, 'hires_data', 'main')
        this.main_map.getLayers().insertAt(0,this.hires_layer);
        MapLib.setDataResolution(this.main_map)

        this.messageService.add({severity:'success', summary:'Hi-Res Data Available', detail:'Render Completed'});
        this.disabled = false
        // hide loading screen
        document.getElementById("spinner-front").classList.remove("show");
        document.getElementById("spinner-back").classList.remove("show");
      }
    })

    // Mannkendall data
    this.dataService.currentMannkendall.subscribe(res => {
      if(res && res.resolution == 'lowres_trend') {
        this.lowres_mannkendall_layer = this.getTrendLayer(res, this.grid_lowres, res.resolution)
        this.main_map.addLayer(this.lowres_mannkendall_layer)
        this.messageService.add({severity:'success', summary:'Low-Res Trend Available', detail:'Render Completed'});
      } else if(res && res.resolution == 'hires_trend') {
        this.hires_mannkendall_layer = this.getTrendLayer(res, this.grid_hires, res.resolution)
        this.main_map.addLayer(this.hires_mannkendall_layer)
        MapLib.setTrendResolution(this.main_map)
        MapLib.setTrendResolution(this.trend_analysis_map)
        this.messageService.add({severity:'success', summary:'Hi-Res Trend Available', detail:'Render Completed'});
      } 
    })

    // Trend analysis data
    this.dataService.currentTrendAnalysis.subscribe(res => {
      if(res && res.resolution == 'lowres_data') {
        this.lowres_trend_analysis_layer = this.getDataLayer(this.details, res, this.grid_lowres, 'lowres_data', 'trend')
        MapLib.clearLayers(this.trend_analysis_map)
        this.trend_analysis_map.getLayers().insertAt(0, this.lowres_trend_analysis_layer);
        // this.trend_analysis_map.addLayer(this.lowres_mannkendall_layer)
        this.trend_analysis_map.getView().setZoom(4)
        this.trend_analysis_map.getView().setCenter([115, 5])
        this.messageService.add({severity:'success', summary:'Low-Res Trend Analysis Data Available', detail:'Render Completed'});
      } else if(res && res.resolution == 'hires_data') {
        this.hires_trend_analysis_layer = this.getDataLayer(this.details, res, this.grid_hires, 'hires_data', 'trend')
        this.trend_analysis_map.getLayers().insertAt(0, this.hires_trend_analysis_layer);
        // this.trend_analysis_map.addLayer(this.hires_mannkendall_layer)
        this.messageService.add({severity:'success', summary:'Hi-Res Trend Analysis Data Available', detail:'Render Completed'});
        MapLib.setDataResolution(this.trend_analysis_map)
      }
    })
  }
}