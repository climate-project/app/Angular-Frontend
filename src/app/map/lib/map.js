import 'ol/ol.css';
import {Map, View} from 'ol';
import TileLayer from 'ol/layer/Tile';
import OSM from 'ol/source/OSM';
import Graticule from 'ol/layer/Graticule';
import Polygon from 'ol/geom/Polygon'

// GeoJson
import GeoJSON from 'ol/format/GeoJSON.js';
import VectorLayer from 'ol/layer/Vector.js';
import VectorSource from 'ol/source/Vector.js';
import {Fill, Stroke, Style, Text} from 'ol/style.js';

import * as d3 from 'd3';

console.log("/map/lib/map.js work!")


export function draw_map(target) {
  // draw country map in background layer
  // GeoJson Vector Style
  var style = new Style({
    
    // fill color in land area
    fill: new Fill({
      color: 'rgba(255, 255, 255, 0)'
    }),

    stroke: new Stroke({
      color: '#000000',
      width: 1
    }),
  })
  // ------------------
  var raster = new TileLayer({
    source: new OSM({
      wrapX: false,
      minZoom: 12,
      maxZoom: 16
    }),
  });

  var mapvectorLayer = new VectorLayer({
    name: "baseLayer",
    source: new VectorSource({
      url: './assets/map/geo-medium.json',
      format: new GeoJSON(),
      wrapX: false,
      minZoom: 2,
      maxZoom: 10
    }),
    style: function(feature) {
      return style;
    }
  });

  const map = new Map({
    target: target,
    layers: [
      mapvectorLayer
    ],
    view: new View({
      projection: 'EPSG:4326',
      center: [0, 0],
      zoom: 2,
      minZoom: 2,
      maxZoom: 12,
      extent: [-180, -90, 180, 90]
    })
  });
  
  return map
}

export function draw_graticule(target) {
  // Create the graticule component
  const minor_gratucule = new Graticule({
    // the style to use for the lines, optional.
    maxLines: 200,
    intervals: [5, 5],
    targetSize: 40,
    strokeStyle: new Stroke({
      color: 'rgba(0,88,212,0.25)',
      width: 0.5,
    }),
  });
  minor_gratucule.setMap(target);

  const major_graticule = new Graticule({
    maxLines: 100,
    intervals: [20, 20],
    targetSize: 40,
    strokeStyle: new Stroke({
      color: 'rgba(0,88,212,0.5)',
      width: 1,
    }),
    showLabels: true
  });
  major_graticule.setMap(target);
}

export function draw_data({
  color_map, 
  geojson, 
  lat_step, 
  lon_step, 
  min, 
  max, 
  legendtarget, 
  layername, 
  legend_title,
  legend_fix
} = {}) {
  // select color 
  // https://observablehq.com/@d3/color-legend
  var color = []; var colorScale;
  if (color_map=='cool_warm') {
    if(legendtarget=='main'){
      color = ["#a50026", "#d73027", "#f46d43", "#fdae61", "#fee090", "#ffffbf", "#e0f3f8", "#abd9e9", "#74add1", "#4575b4", "#313695"].reverse()
      colorScale = d3.scaleQuantile([min, max], color)
    } else if(legendtarget=='trend'){
      color = ["#ca0020", "#f4a582", "#f7f7f7", "#92c5de", "#0571b0"].reverse()
      colorScale = d3.scaleQuantile([min, 0, max], color)
    }
  } else if (color_map=='warm_cool') {
    if(legendtarget=='main'){
      color = ["#a50026", "#d73027", "#f46d43", "#fdae61", "#fee090", "#ffffbf", "#e0f3f8", "#abd9e9", "#74add1", "#4575b4", "#313695"]  //d3.schemeRdYlBu[11]
      colorScale = d3.scaleQuantile([min, max], color)
    } else if(legendtarget=='trend'){
      color = ["#ca0020", "#f4a582", "#f7f7f7", "#92c5de", "#0571b0"]  //d3.schemeRdBu[5]
      colorScale = d3.scaleQuantile([min, 0, max], color)
    }
  } else if (color_map=='dry_wet') {
    if(legendtarget=='main'){
      color = ["#f7fcf0", "#e0f3db", "#ccebc5", "#a8ddb5", "#7bccc4", "#4eb3d3", "#2b8cbe", "#0868ac", "#084081"]  //d3.schemeGnBu[9]
      if(legend_fix){colorScale = d3.scaleQuantile([min, max], color)}
      else{colorScale = d3.scaleQuantile([0, max], color)}
    } else if(legendtarget=='trend'){
      color = ["#a6611a", "#dfc27d", "#f5f5f5", "#80cdc1", "#018571"]  //d3.schemeBrBG[5]
      colorScale = d3.scaleQuantile([min, 0, max], color)
    }
  } else if (color_map=='wet_dry') {
    if(legendtarget=='main'){
      color = ['#714108', '#8d520b', '#a96c1e', '#c28633', '#d3aa5f', '#e2c787', '#efdcad', '#f6ebcd', '#f5f2e8'].reverse()
      if(legend_fix){colorScale = d3.scaleQuantile([min, max], color)}
      else{colorScale = d3.scaleQuantile([0, max], color)}
    } else if(legendtarget=='trend'){
      color = ["#a6611a", "#dfc27d", "#f5f5f5", "#80cdc1", "#018571"].reverse()
      colorScale = d3.scaleQuantile([min, 0, max], color)
    }
  } 

  if(legendtarget=='main') {
    createLegend({
    color: colorScale,
    min: min,
    max: max,
    target: legendtarget,
    title: legend_title,
    tickFormat: ".2f"});
  } else if(legendtarget=='trend') {
    createLegend({
    color: colorScale,
    min: min,
    max: max,
    target: legendtarget,
    title: legend_title,
    tickFormat: ".3f"});
  }

  var gridStyle = function (feature) {
  
    var coordinate = feature.getGeometry().getCoordinates(),
      x = coordinate[0]- lon_step / 2,
      y = coordinate[1]- lat_step / 2,
      pop = parseInt(feature.getProperties().value),
      rgb = d3.rgb(colorScale(pop));
    if (isNaN(pop)) { return }
    return [
      new Style({
        fill: new Fill({
          color: [rgb.r, rgb.g, rgb.b, 1]
        }),
        // stroke: new Stroke({
        //   color: [0, 0, 0, 0.8],
        //   width: 0.2,
        // }),
        geometry: new Polygon([[
            [x,y], [x, y + lat_step], [x + lon_step, y + lat_step], [x + lon_step, y], [x,y]
        ]]),
      })
      
    ];
  };
  
  var grid = new VectorSource({
    wrapX: false,
    extent: [-180, -90, 180, 90],
    features: (new GeoJSON()).readFeatures(geojson)
  });
  
  var gridLayer = new VectorLayer({
      name: layername,
      source: grid,
      style: gridStyle
  });

  return gridLayer
}

export function draw_trend(geojson, layername='') {
  var createTextStyle = function (feature) {
    return new Text({
      textAlign: "center",
      text: feature.getProperties().trend,
      fill: new Fill({color: "black"}),
      stroke: new Stroke({color: "black", width: 1}),
      placement: "point",
    });
  };

  var grid = new VectorSource({
    wrapX: false,
    extent: [-180, -90, 180, 90],
    features: (new GeoJSON()).readFeatures(geojson)
  });
  
  var gridLayer = new VectorLayer({
      name: layername,
      source: grid,
      style: function(feature) {
        return new Style({
          text: createTextStyle(feature)
        })
      }
  });

  return gridLayer
}

// Based on https://observablehq.com/@d3/color-legend
function createLegend ({
  color,
  min,
  max,
  target,
  title,
  tickSize = 6,
  width = 700, 
  height = 44 + tickSize,
  marginTop = 18,
  marginRight = 0,
  marginBottom = 16 + tickSize,
  marginLeft = 0,
  ticks = width / 64,
  tickFormat,
  tickValues
} = {}) {
  if(target == "trend") {width = 480}
  let tickAdjust = g => g.selectAll(".tick line").attr("y1", marginTop + marginBottom - height);
  const thresholds
        = color.thresholds ? color.thresholds() // scaleQuantize
        : color.quantiles ? color.quantiles() // scaleQuantile
        : color.domain(); // scaleThreshold
  const thresholdFormat
        = tickFormat === undefined ? d => d
        : typeof tickFormat === "string" ? d3.format(tickFormat)
        : tickFormat;
  var x = d3.scaleLinear()
        .domain([-1, color.range().length - 1])
        .rangeRound([marginLeft, width - marginRight]);
  
  var svg = d3.select('svg.'+target+'.legend')
  svg.selectAll('rect').remove();
  svg.selectAll("g").remove();
  // fill color
  svg.append("g")
      .selectAll("rect")
      .data(color.range())
      .join("rect")
        .attr("x", (d, i) => x(i - 1))
        .attr("y", marginTop)
        .attr("width", (d, i) => x(i) - x(i - 1))
        .attr("height", height - marginTop - marginBottom)
        .attr("fill", d => d);
  
  tickValues = d3.range(thresholds.length); 
  tickFormat = i => thresholdFormat(thresholds[i], i);

  // tick, tick value
  svg.append("g")
      .attr("transform", `translate(0,${height - marginBottom})`)
      .attr("class", "tick")
      .call(d3.axisBottom(x)
        .ticks(ticks, typeof tickFormat === "string" ? tickFormat : undefined)
        .tickFormat(typeof tickFormat === "function" ? tickFormat : undefined)
        .tickSize(tickSize)
        .tickValues(tickValues))
      .call(tickAdjust)
      .call(g => g.select(".domain").remove())
      .call(g => g.append("text")
        .attr("x", marginLeft)
        .attr("y", marginTop + marginBottom - height - 6)
        .attr("fill", "currentColor")
        .attr("text-anchor", "start")
        .attr("font-weight", "bold")
        .text(title));
}

export function merge_data_to_geojson(geojson, data, type){
  var tempData = data.flat();
  var trend = "";
  if(type == "value"){
    for(let i=0; i<geojson.features.length; i++) {
      geojson["features"][i]["properties"] = {"value": tempData[i]}
    }
  }else if (type == "trend"){
    for(let i=0; i<geojson.features.length; i++) {
      if(tempData[i] == 0) { trend = ""}
      else if(tempData[i] == 1) { trend = "+"}
      else if(tempData[i] == -1) { trend = "-"}
      geojson["features"][i]["properties"] = {"trend": trend}
    }
  }
  return geojson
}

export function clearLayers(map){
  var layersToRemove = [];
  map.getLayers().forEach(function (layer) {
    if (layer.get('name') == 'lowres_data' || layer.get('name') == 'hires_data' || layer.get('name') == 'lowres_trend' || layer.get('name') == 'hires_trend') {
      layersToRemove.push(layer);
    }
  });

  for(var i = 0; i < layersToRemove.length; i++) {
    map.removeLayer(layersToRemove[i]);
  }
}

export function setDataResolution(map){
  map.getLayers().forEach(function (layer) {
    if (layer.get('name') == 'lowres_data') {
      layer.setVisible(true)
      layer.set('minZoom', 0)
      layer.set('maxZoom', 6)
    }
    else if (layer.get('name') == 'hires_data') {
      layer.set('minZoom', 6)
      layer.set('maxZoom', 12)
    }
  });
}

export function setTrendResolution(map){
  map.getLayers().forEach(function (layer) {
    if (layer.get('name') == 'lowres_trend') {
      layer.set('minZoom', 0)
      layer.set('maxZoom', 6)
    }
    else if (layer.get('name') == 'hires_trend') {
      layer.set('minZoom', 6)
      layer.set('maxZoom', 12)
    }
  });
}