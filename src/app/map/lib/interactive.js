import Select from 'ol/interaction/Select.js';
import {Stroke, Style} from 'ol/style.js';

export function select_country(targetMap, unit) {
  var features, baselayer ;
  targetMap.getLayers().forEach(function(layers) {
    if (layers.get('name') === 'lowres_data') {
      features = layers.getSource().getFeatures()
      if(layers.get('name') === 'hires_data'){
        features = layers.getSource().getFeatures()
      }
    } else if (layers.get('name') === 'baseLayer') {
      baselayer = layers
    }
  })

  var selectedStyle = new Style({
    stroke: new Stroke({
      color: '#2196f3',
      width: 3
    })
  });

  var select = new Select({
    layers: [baselayer],
    style: selectedStyle
  });
  targetMap.addInteraction(select);

  select.on('select', function(e) {
    if(e.selected.length != 0) {
      var poly = e.selected[0].getGeometry()
      var valueArr = []; var tmpArr = [];

      for (var i = 0; i < features.length; i++) {
        if (poly.intersectsExtent(features[i].getGeometry().getExtent())) {
          valueArr.push(features[i].getProperties().value);
        }
      }
      var min = Math.round(Math.min.apply(null, valueArr)*100)/100;
      var max = Math.round(Math.max.apply(null, valueArr)*100)/100;
      var avg = Math.round(valueArr.reduce((a,b) => a + b, 0) / valueArr.length *100 )/100
      valueArr.forEach(function(value) {
        tmpArr.push(Math.round(Math.pow(value-avg, 2)*100)/100)
      })
      var variance = Math.round(tmpArr.reduce((a,b) => a + b, 0) / tmpArr.length *100 )/100

      document.getElementById('country').innerHTML = '(' + e.selected[0].get('name') + ')';
      document.getElementById('stat_avg').innerHTML = 'Avg: ' + avg + '&nbsp;' +  unit
      document.getElementById('stat_min').innerHTML = 'Min: ' + min + '&nbsp;' +  unit
      document.getElementById('stat_max').innerHTML = 'Max: ' + max + '&nbsp;' +  unit
      document.getElementById('stat_variance').innerHTML = 'Variance: ' + variance + '&nbsp;' +  unit
      }
  }); 
  return select
}